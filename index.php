/***********the language used was php version: '7.2.18' ***********/
<?php
//'crawl_page_print' function is the one in charge of printing the links on the screen 
//the function takes in an url, a number and a format parameter to select how to print the result on the screen
function crawl_page_print($url, $depth, $printFormat)
{
	$allRefArray = array();
	//invoking the 'crawl_page' function to get all the links from the url
	$allRefArray = crawl_page($url,$depth);
	if(sizeof($allRefArray) > 0){
		//removing duplicates from array
		$allRefArray = array_unique($allRefArray);
		$linkRefArray = array();
		$badRefArray = array();
		//iterate over all the links to filter the good and bad links
		foreach($allRefArray as $linkStr){
			if (filter_var($linkStr, FILTER_VALIDATE_URL) === FALSE) 
				array_push($badRefArray,$linkStr); 
			else
				array_push($linkRefArray,$linkStr);
		}
		//create an object with two arrays as param
		$object = (object) [
			'Links' => $linkRefArray,
			'Bad-ref' => $badRefArray,
		];
		//printing the object on the screen , it could be print as a object or a json object
		if($printFormat === 0)
			//print it as an object
			var_dump($object);
		else
			//print it as a json object
			var_dump( json_encode($object));
	}
	else{
		$msg = "No Links found";
		var_dump($msg);
	}
}
//'crawl_page' function is used to crawl links on a website , 
function crawl_page($url, $depth)
{
    $seenArray = array();
	$allHrefArray = array();
	//if $depth = 0 and  $url  is null or no valid return an empty array
    if (isset($seenArray[$url]) || $depth === 0 || filter_var($url, FILTER_VALIDATE_URL) === FALSE ) {
        return $allHrefArray;
    }
    $seenArray[$url] = true;
	//Create a DOM parser object
    $dom = new DOMDocument('1.0');
	//Parse the HTML from the url.
    @$dom->loadHTMLFile($url);
	//get all the <a> tags
    $hrefElem = $dom->getElementsByTagName('a');
	//Iterate over all the <a> tags
	foreach ($hrefElem as $element) {
		//getting the <a href>
		$linkStr = $element->getAttribute('href');
		//adding all the links in an array to return it
		if (!in_array($linkStr, $allHrefArray)) {	
			array_push($allHrefArray, $linkStr);
			$hrefArray = array();
			$hrefArray = crawl_page($linkStr, $depth - 1);	
			$allHrefArray = array_merge($allHrefArray, (array)$hrefArray);		
		}		
	}
	return $allHrefArray; 
}
crawl_page_print("https://demos.creative-tim.com/", 1, 0);
?>